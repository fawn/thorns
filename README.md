# thorns

a cute n cozy lil palette

![Palette](palette.png)

soontm

```css
:root {
    --thorns-accent-purple: #d895ee;
    --thorns-accent-pink: #ee95d2;

    --thorns-bg0: #151515;
    --thorns-bg1: #505050;

    --thorns-fg0: #d8d0d5;
    --thorns-fg1: #909090;

    --thorns-red: #ee9598;
    --thorns-orange: #e8b197;
    --thorns-yellow: #e8d097;
    --thorns-green: #9be099;
    --thorns-cyan: #97d0e8;
    --thorns-blue: #979ae8;
    --thorns-purple: #ca97e8;

    --thorns-dim-red: #c76f72;
    --thorns-dim-orange: #c18b72;
    --thorns-dim-yellow: #c1a972;
    --thorns-dim-green: #77b975;
    --thorns-dim-cyan: #72a9c1;
    --thorns-dim-blue: #7275c1;
    --thorns-dim-purple: #a372c1;
}
```
